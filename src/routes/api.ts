import { Router } from 'express';
import setup from './data/setup';
import authService from "@services/auth-service";
import app from "@server";
import authRouter from "@routes/auth-router";


const apiRouter = Router();


// Setup routers
setup.registerRouters(apiRouter);

let clientsDataByRoute = setup.clientsDataByRoute;

async function isAdminMiddleware(req: any,res: any,next: any) {
    //all routes beginning with /admin will need admin token
    const client =  clientsDataByRoute.get(authRouter.baseRoute)!.client;
    const token = await authService.getAuthToken(req);
    if (!token) {
        res.status(401).send({ success: false, message: "Unauthorized" });
        return;
    }
    const isAdmin = await authService.isAdmin(token);

    if (!isAdmin) {
        res.status(401).send({ success: false, message: "Unauthorized" });
        return;
    }

    const isTokenValid = await authService.isValid(client,token);
    if (!isTokenValid) {
        res.status(401).send({ success: false, message: "Unauthorized" });
        return;
    }

    next();
}


async function isDelivererMiddleware(req: any,res: any,next: any) {
    //all routes beginning with /admin will need admin token
    const client =  clientsDataByRoute.get(authRouter.baseRoute)!.client;
    const token = await authService.getAuthToken(req);
    if (!token) {
        res.status(401).send({ success: false, message: "Unauthorized" });
        return;
    }
    const isDelivererOrAdmin = await authService.isDeliverer(token) || await authService.isAdmin(token);

    if (!isDelivererOrAdmin) {
        res.status(401).send({ success: false, message: "Unauthorized" });
        return;
    }

    const isTokenValid = await authService.isValid(client,token);
    if (!isTokenValid) {
        res.status(401).send({ success: false, message: "Unauthorized" });
        return;
    }
    next();
}

async function isOwnerMiddleware(req: any,res: any,next: any) {

    const client =  clientsDataByRoute.get(authRouter.baseRoute)!.client;
    const token = await authService.getAuthToken(req);
    if (!token) {
        res.status(401).send({ success: false, message: "Unauthorized" });
        return;
    }
    const isOwnerOrAdmin = await authService.isOwner(token) || await authService.isAdmin(token);

    if (!isOwnerOrAdmin) {
        res.status(401).send({ success: false, message: "Unauthorized" });
        return;
    }

    const isTokenValid = await authService.isValid(client,token);
    if (!isTokenValid) {
        res.status(401).send({ success: false, message: "Unauthorized" });
        return;
    }
    next();
}

export default {
    apiRouter,
    clientsDataByRoute,
    isAdminMiddleware,
    isDelivererMiddleware,
    isOwnerMiddleware
} as const;