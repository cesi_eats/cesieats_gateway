import StatusCodes from 'http-status-codes';
import express, { Request, Response, Router } from 'express';

import { ParamMissingError } from '@utils/errors';
import {ClientData} from "@routes/data/ClientData";
import api from "@routes/api";
import imageService from "@services/image-service";
import multer from 'multer';
import fileUpload from "express-fileupload";

const baseRoute = "/article";


// Constants
const router = Router();


router.use(express.static('public'));
router.use(fileUpload());

const { CREATED, OK } = StatusCodes;


// Paths
export const p = {
    all: {endpoint : '/', target : '/api/article'},
    add: {endpoint : '/owner/add/:categoryId', target : '/api/article/:categoryId'},
    get: {endpoint : '/:id', target : '/api/article/:id'},
    delete: {endpoint : '/owner/:id', target : '/api/article/:id'},
    update : {endpoint : '/owner/:id', target : '/api/article/:id'},
    getGroupByCategory: {endpoint : '/grouped/:cardId', target : '/api/article/grouped/:cardId'}
} as const;


//all routes beginning with /owner will need owner token
router.use('/owner', async function(req, res, next) {
    await api.isOwnerMiddleware(req, res, next);
});

/**
 * @swagger
 * /api/article:
 *  get:
 *     description: Gets all articles
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: All articles
 */
router.get(p.all.endpoint, async (_: Request, res: Response) => {
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.get(p.all.target).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    })
});

/**
 * @swagger
 * /api/article/:articleId/:
 *   get:
 *      description: Adds an article to a category
 *      parameters:
 *        - name: articleId
 *      produces:
 *       - application/json
 *      responses:
 *        '200':
 *          description: An article
 */
router.get(p.get.endpoint, async (req: Request, res: Response) => {
    const id = req.params.id;
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.get(p.get.target.replace(":id",id)).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    });
});


/**
 * @swagger
 * /api/article/add/:categoryId:
 *   post:
 *      description: Adds an article to a category
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: name
 *       - name: description
 *       - name: price
 *       - name: image
 *      responses:
 *        '200':
 *          description: The created article
 */
router.post(p.add.endpoint, async (req: Request, res: Response) => {

    //TODO check if admin or owner of the restaurant the article belongs to
    try {
        const body = JSON.parse(req.body.content);
        const categoryId = req.params.categoryId;

        const imageResponse = await imageService.postImage(req);

        if (!imageResponse.success || !imageResponse.url){
            console.log("could not upload image");
        }
        let requestBody;
        if (imageResponse.success || imageResponse.url){
            const url = imageResponse.url;
            requestBody= {
                ...body,
                img: url,
            }
        }
        else{
            requestBody= {
                ...body,
            }
        }
        // @ts-ignore
        api.clientsDataByRoute.get(baseRoute).client.post(p.add.target.replace(":categoryId",categoryId),requestBody).then((value: { body: object; }) => {
            // @ts-ignore
            const body = value.data;
            // @ts-ignore
            const status = value.status;

            res.status(status).send(body);
        });
    }
    catch (e){
        console.log(e);
        res.status(500).send({ success: false, message: "Server error" });
    }

});

/**
 * @swagger
 * /api/article/:articleId:
 *   delete:
 *      description: Deletes an article
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: articleId
 *      responses:
 *        '200':
 *          description: Whether the article was deleted or not
 */
router.delete(p.delete.endpoint, async (req: Request, res: Response) => {

    //TODO check if admin or owner of the restaurant the article belongs to
    // + remove image from azure

    const id = req.params.id;
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.delete(p.delete.target.replace(":id",id)).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    });
});

/**
 * @swagger
 * /api/article/:articleId:
 *   put:
 *      description: Updates an article
 *      produces:
 *       - application/json
 *      parameters:
 *        - name: name
 *        - name: description
 *        - name: price
 *        - name: image
 *      responses:
 *        '200':
 *          description: The updated article
 */
router.put(p.update.endpoint, async (req: Request, res: Response) => {

    //TODO check if admin or owner of the restaurant the article belongs to
    try {
        const body = JSON.parse(req.body.content);
        const id = req.params.id;


        const imageResponse = await imageService.postImage(req);
        let requestBody;
        if (imageResponse.success || imageResponse.url){
            const url = imageResponse.url;
            requestBody= {
                ...body,
                img: url,
            }
        }
        else{
            requestBody= {
                ...body,
            }
        }
        // @ts-ignore
        api.clientsDataByRoute.get(baseRoute).client.put(p.update.target.replace(":id",id),requestBody).then((value: { body: object; }) => {
            // @ts-ignore
            const body = value.data;
            // @ts-ignore
            const status = value.status;

            res.status(status).send(body);
        }).catch((e: any) => {
            console.log(e);
        });
    }
    catch (e){
        console.log(e);
        res.status(500).send({ success: false, message: "Server error" });
    }
});

/**
 * @swagger
 * /api/article/grouped/:cardId:
 *   get:
 *      description: Gets all articles from a card grouped by category
 *      produces:
 *       - application/json
 *      parameters:
 *        - name: cardId
 *      responses:
 *        '200':
 *          description: The articles grouped by category
 */
router.get(p.getGroupByCategory.endpoint, async (req: Request, res: Response) => {
    const cardId = req.params.cardId;
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.get(p.getGroupByCategory.target.replace(":cardId",cardId)).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    });
});

// Export default
export default {
    router,
    baseRoute
} as const;
