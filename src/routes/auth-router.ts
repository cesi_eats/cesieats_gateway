import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';

import { ParamMissingError } from '@utils/errors';
import {ClientData} from "@routes/data/ClientData";
import api from "@routes/api";
import authService from "@services/auth-service";
const swaggerJsdoc = require('swagger-jsdoc');
const baseRoute = "/auth";


// Constants
const router = Router();
const { CREATED, OK } = StatusCodes;

// Paths
export const p = {
    all: {endpoint : '/admin/all', target : '/api/auth/'},
    register: {endpoint : '/register', target : '/api/auth/register'},
    login: {endpoint : '/login', target : '/api/auth/login'},
    update: {endpoint : '/admin/update', target : '/api/auth/update'},
    delete:  {endpoint : '/admin/delete', target : '/api/auth/delete'},
    isValid: {endpoint : '/isValid', target : '/api/auth/isValid'},
    isAdmin: '/api/auth/isAdmin',
    isDeliverer: '/api/auth/isDeliverer',
    isOwner: '/api/auth/isOwner',
    details: '/api/auth/user',
} as const;

//all routes beginning with /admin will need admin token
router.use('/admin', async function(req, res, next) {
    await api.isAdminMiddleware(req, res, next);
});

/**
 * @swagger
 * /api/auth/admin/all:
 *   post:
 *      description: Retrieve all users (admin)
 *      produces:
 *       - application/json
 *      responses:
 *        '200':
 *          description: Json containing the users
 */
router.get(p.all.endpoint, async (req: Request, res: Response) => {
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.get(p.all.target).then((value: { body: any; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    }).catch((err: any) => {
        console.error(err);
        res.status(500).send({ success: false, message: "Server error" });
    });
});


/**
 * @swagger
 * /api/auth/login:
 *   post:
 *      description: Retrieve a JWT token for the user if the credentials are valid
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: email
 *       - name: password
 *      responses:
 *        '200':
 *          description: Json containing the token
 */
router.post(p.login.endpoint, async (req: Request, res: Response) => {
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.post(p.login.target, req.body).then((value: { body: any; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    }).catch((err: any) => {
        console.error(err);
        res.status(500).send({ success: false, message: "Server error" });
    });
});

/**
 * @swagger
 * /api/auth/register:
 *   post:
 *      description: Registers a new user
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: firstName
 *       - name: name
     *       - name: email
     *       - name: password
     *      responses:
 *        '200':
 *          description: Whether the user was created or not
 */
router.post(p.register.endpoint, async (req: Request, res: Response) => {
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.post(p.register.target, req.body).then((value: { body: any; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    }).catch((err: any) => {
        console.error(err);
        res.status(500).send({ success: false, message: "Server error" });
    });
});

/**
 * @swagger
 * /api/auth/update:
 *   put:
 *      description: Updates an existing user
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: firstName
 *       - name: name
 *       - name: email
 *       - name: password
 *      responses:
 *        '200':
 *          description: Whether the user was created or not
 */
router.put(p.update.endpoint, async (req: Request, res: Response) => {

    const client =  api.clientsDataByRoute.get(baseRoute)!.client;
    // @ts-ignore
    client.put(p.update.target, req.body).then((value: { body: any; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    }).catch((err: any) => {
        console.error(err);
        res.status(500).send({ success: false, message: "Server error" });
    });
});

/**
 * @swagger
 * /api/auth/delete:
 *   post:
 *      description: Deletes a user
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: email
 *      responses:
 *        '200':
 *          description: Whether the user was created or not
 */
router.post(p.delete.endpoint, async (req: Request, res: Response) => {

    const client =  api.clientsDataByRoute.get(baseRoute)!.client;
    client.delete(p.delete.target, { data : req.body }).then((value: { body: any; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    }).catch((err: any) => {
        console.error(err);
        res.status(500).send({ success: false, message: "Server error" });
    });
});

router.post(p.isValid.endpoint, async (req: Request, res: Response) => {
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.post(p.isValid.target, req.body).then((value: { body: any; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    }).catch((err: any) => {
        console.error(err);
        res.status(500).send({ success: false, message: "Server error" });
    });
});


// Export default
export default {
    router,
    baseRoute
} as const;
