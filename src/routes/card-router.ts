import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';

import { ParamMissingError } from '@utils/errors';
import {ClientData} from "@routes/data/ClientData";
import api from "@routes/api";

const baseRoute = "/card";


// Constants
const router = Router();
const { CREATED, OK } = StatusCodes;

// Paths
export const p = {
    all: {endpoint : '/', target : 'api/card'},
    add: {endpoint : '/:restaurantId', target : '/api/card/:restaurantId'},
    get: {endpoint : '/:cardId', target : '/api/card/:cardId'},
    update : {endpoint : '/:cardId', target : '/api/card/:cardId'},
    delete: {endpoint : '/:cardId', target : '/api/card/:cardId'},
} as const;


/**
 * @swagger
 * /api/card:
 *  get:
 *     description: Gets all cards
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: All cards
 */
router.get(p.all.endpoint, async (req: Request, res: Response) => {
    const id = req.params.id;
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.get(p.all.target).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    }).catch((err: any) => {
        console.log(err);
        res.sendStatus(500);
    });
});

/**
 * @swagger
 * /api/card/cardId:
 *  get:
 *     description: Gets a card by id
 *     parameters:
 *       - name: cardId
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: The card
 */
router.get(p.get.endpoint, async (req: Request, res: Response) => {

    const cardId = req.params.cardId;
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.get(p.get.target.replace(":cardId",cardId)).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    }).catch((err: any) => {
        console.log(err);
        res.sendStatus(500);
    });
});


/**
 * @swagger
 * /api/card/:restaurantId/:
 *  post:
 *     description: Adds a card to a given restaurant
 *     parameters:
 *       - name: restaurantId
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: The created card
 */
router.post(p.add.endpoint, async (req: Request, res: Response) => {

    //TODO check if admin or owner of the restaurant the category belongs to

    const restaurantId = req.params.restaurantId;
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.post(p.add.target.replace(":restaurantId",restaurantId), req.body).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    }).catch((err: any) => {
        console.log(err);
    });
});


/**
 * @swagger
 * /api/card/:cardId/:
 *  post:
 *     description: Updates a card by its id
 *     parameters:
 *       - name: cardId
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: The updated card
 */
router.put(p.update.endpoint, async (req: Request, res: Response) => {

    //TODO check if admin or owner of the restaurant the category belongs to

    const cardId = req.params.cardId;
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.post(p.update.target.replace(":cardId",cardId), req.body).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    }).catch((err: any) => {
        console.log(err);
    });
});



/**
 * @swagger
 * /api/category/:cardId/:
 *  delete:
 *     description: Deletes a card by its id
 *     parameters:
 *       - name: cardId
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 */
router.delete(p.delete.endpoint, async (req: Request, res: Response) => {

    //TODO check if admin or owner of the restaurant the category belongs to

    const cardId = req.params.cardId;
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.delete(p.delete.target.replace(":cardId",cardId)).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    }).catch((err: any) => {
        console.log(err);
        res.sendStatus(500);
    });
});



// Export default
export default {
    router,
    baseRoute
} as const;
