import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';

import { ParamMissingError } from '@utils/errors';
import {ClientData} from "@routes/data/ClientData";
import api from "@routes/api";

const baseRoute = "/category";


// Constants
const router = Router();
const { CREATED, OK } = StatusCodes;

// Paths
export const p = {
    all: {endpoint : '/', target : '/api/category'},
    add: {endpoint : '/owner/:restaurantId', target : '/api/category/:restaurantId'},
    get: {endpoint : '/:categoryId', target : '/api/category/:categoryId'},
    update : {endpoint : '/owner/:categoryId', target : '/api/category/:categoryId'},
    delete: {endpoint : '/owner/:categoryId', target : '/api/category/:categoryId'},
} as const;

//all routes beginning with /owner will need owner token
router.use('/owner', async function(req, res, next) {
    await api.isOwnerMiddleware(req, res, next);
});

/**
 * @swagger
 * /api/category:
 *  get:
 *     description: Gets all categories
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: All categories
 */
router.get(p.all.endpoint, async (req: Request, res: Response) => {
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.get(p.all.target).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    }).catch((err: any) => {
        console.log(err);
        res.sendStatus(500);
    });
});

/**
 * @swagger
 * /api/category:
 *  get:
 *     description: Gets a category by id
 *     parameters:
 *       - name: categoryId
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: The category
 */
router.get(p.get.endpoint, async (req: Request, res: Response) => {

    const categoryId = req.params.categoryId;
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.get(p.get.target.replace(":categoryId",categoryId)).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    }).catch((err: any) => {
        console.log(err);
        res.sendStatus(500);
    });
});


/**
 * @swagger
 * /api/category/:cardId/:
 *  post:
 *     description: Adds a category to a given card (menu)
 *     parameters:
 *       - name: cardId
 *       - name: categoryName
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: The created category
 */
router.post(p.add.endpoint, async (req: Request, res: Response) => {

    //TODO check if admin or owner of the restaurant the category belongs to

    const restaurantId = req.params.restaurantId;
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.post(p.add.target.replace(":restaurantId",restaurantId), req.body).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    }).catch((err: any) => {
        console.log(err);
        res.sendStatus(500);
    });
});


/**
 * @swagger
 * /api/category/:cardId/:
 *  post:
 *     description: Updates a category by its id
 *     parameters:
 *       - name: cardId
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: The updated category
 */
router.put(p.update.endpoint, async (req: Request, res: Response) => {

    //TODO check if admin or owner of the restaurant the category belongs to

    const categoryId = req.params.categoryId;
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.put(p.update.target.replace(":categoryId",categoryId), req.body).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    }).catch((err: any) => {
        console.log(err);
        res.sendStatus(500);
    });
});



/**
 * @swagger
 * /api/category/:cardId/:
 *  delete:
 *     description: Deletes a category by its id
 *     parameters:
 *       - name: cardId
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 */
router.delete(p.delete.endpoint, async (req: Request, res: Response) => {

    //TODO check if admin or owner of the restaurant the category belongs to

    const categoryId = req.params.categoryId;
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.delete(p.delete.target.replace(":categoryId",categoryId)).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    }).catch((err: any) => {
        console.log(err);
        res.sendStatus(500);
    });
});



// Export default
export default {
    router,
    baseRoute
} as const;
