import {Router} from "express";

const axios = require("axios");

export class ClientData{


    constructor(server : string, router: Router, path: string) {
        this._client = axios.create({
            baseURL: server,
            timeout: 5000,
            validateStatus : (status: number) => {
                return status >= 200 && status < 500;
            }
        });
        this._server = server;
        this._router = router;
        this._path = path;
    }

    private readonly _client: typeof axios;
    private readonly _router: Router;
    private readonly _path: string;
    private readonly _server: string;


    get client(): typeof axios {
        return this._client;
    }

    get server(): string {
        return this._server;
    }

    get router(): Router {
        return this._router;
    }

    get path(): string {
        return this._path;
    }
}