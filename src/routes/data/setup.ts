import {Router} from "express";
import {ClientData} from "@routes/data/ClientData";
const Resilient = require("resilient");
import articlesRouter from "@routes/articles-router";
import imagesRouter from "@routes/images-router";
import authRouter from "@routes/auth-router";
import categoryRouter from "@routes/category-router";
import restaurantRouter from "@routes/restaurant-router";
import orderRouter from "@routes/order-router";
import cardRouter from "@routes/card-router";

let clientsDataByRoute = new Map<string,ClientData>();

if (!process.env.ARTICLES_SERVICE_URL || !process.env.IMAGES_SERVICE_URL || !process.env.AUTH_SERVICE_URL) {
    throw new Error("Missing environment variables");
}

let articlesClient = new ClientData(
    process.env.ARTICLES_SERVICE_URL,
    articlesRouter.router,
    articlesRouter.baseRoute);

let categoryClient = new ClientData(
    process.env.ARTICLES_SERVICE_URL,
    categoryRouter.router,
    categoryRouter.baseRoute);

let cardClient = new ClientData(
    process.env.ARTICLES_SERVICE_URL,
    cardRouter.router,
    cardRouter.baseRoute);


let restaurantClient = new ClientData(
    process.env.ARTICLES_SERVICE_URL,
    restaurantRouter.router,
    restaurantRouter.baseRoute);

let imagesClient = new ClientData(
    process.env.IMAGES_SERVICE_URL,
    imagesRouter.router,
    imagesRouter.baseRoute);

let orderClient = new ClientData(
    process.env.ORDERS_SERVICE_URL!,
    orderRouter.router,
    orderRouter.baseRoute);


let authClient = new ClientData(
    process.env.AUTH_SERVICE_URL,
    authRouter.router,
    authRouter.baseRoute);

function registerRouters(apiRouter: Router) {
    clientsDataByRoute.forEach(clientData => {
        apiRouter.use(clientData.path, clientData.router);
        console.log(`Registered router ${clientData.path}`);
    });
}

clientsDataByRoute.set(articlesRouter.baseRoute, articlesClient);
clientsDataByRoute.set(imagesRouter.baseRoute, imagesClient);
clientsDataByRoute.set(authRouter.baseRoute, authClient);
clientsDataByRoute.set(categoryRouter.baseRoute, categoryClient);
clientsDataByRoute.set(restaurantRouter.baseRoute, restaurantClient);
clientsDataByRoute.set(orderRouter.baseRoute, orderClient);
clientsDataByRoute.set(cardRouter.baseRoute, cardClient);

export default {
    registerRouters,
    clientsDataByRoute,
    imagesClient
} as const;
