import StatusCodes from 'http-status-codes';
import {Request, Response, Router} from 'express';
import api from "@routes/api";
import fileUpload from 'express-fileupload';
import FormData from 'form-data';

const baseRoute = "/images";



// Constants
const router = Router();
const { CREATED, OK } = StatusCodes;

router.use(fileUpload());

// Paths
export const p = {
    add: {endpoint : '/add', target : '/api/images'},
} as const;


/**
 * Get all users.
 */
router.post(p.add.endpoint, async (req: any, res: Response) => {

    const client = api.clientsDataByRoute.get(baseRoute)!.client;
    const { headers, files } = req;
    const { data, name } = files['img'];
    const formFile = new FormData();
    formFile.append('img', data,  name );
    headers['Content-Type'] = 'multipart/form-data';

    client.post(p.add.target, formFile, {}, headers).then((value: { body: any; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    }).catch((err: any) => {
        res.sendStatus(500);
    });
});


// Export default
export default {
    router,
    baseRoute
} as const;
