import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';

import { ParamMissingError } from '@utils/errors';
import {ClientData} from "@routes/data/ClientData";
import api from "@routes/api";

const baseRoute = "/order";


// Constants
const router = Router();
const { CREATED, OK } = StatusCodes;

// Paths
export const p = {
    all: {endpoint : '/', target : '/api/order'},
    add: {endpoint : '/', target : '/api/order/'},
    accept: {endpoint : '/accept', target : '/api/order/accept'},
    available: {endpoint : '/available', target : '/api/order/available'},
    deliver: {endpoint : '/deliver/:orderId', target : '/api/order/deliver/:orderId'},
    getByUserId: {endpoint : '/user/:userId', target : '/api/order/user/:userId'},
    getByDelivererId: {endpoint : '/deliverer/:delivererId', target : '/api/order/deliverer/:delivererId'},
} as const;

//TODO security checks



/**
 * @swagger
 * /api/order/:
 *   get:
 *      description: Deletes a user
 *      produces:
 *       - application/json
 *      responses:
 *        '200':
 *          description: All orders
 */
router.get(p.all.endpoint, async (_: Request, res: Response) => {

    api.clientsDataByRoute.get(baseRoute)!.client.get(p.all.target).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;
        res.status(status).send(body);
    })
});

/**
 * @swagger
 * /api/order/:
 *   post:
 *      description: Creates an order
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: userId
 *       - name: articles
 *      responses:
 *        '200':
 *          description: The created order
 */
router.post(p.add.endpoint, async (req: Request, res: Response) => {

    api.clientsDataByRoute.get(baseRoute)!.client.post(p.add.target, req.body).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;
        res.status(status).send(body);
    })
});

/**
 * @swagger
 * /api/order/accept:
 *   post:
 *      description: Accepts and order
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: orderId
 *       - name: delivererId
 *      responses:
 *        '200':
 *          description: The accepted order
 */
router.post(p.accept.endpoint, async (req: Request, res: Response) => {

    api.clientsDataByRoute.get(baseRoute)!.client.post(p.accept.target, req.body).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;
        res.status(status).send(body);
    })
});

/**
 * @swagger
 * /api/order/available:
 *   get:
 *      description: Lists all available orders
 *      produces:
 *       - application/json
 *      responses:
 *        '200':
 *          description: All available orders
 */
router.get(p.available.endpoint, async (_: Request, res: Response) => {

    api.clientsDataByRoute.get(baseRoute)!.client.get(p.available.target).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;
        res.status(status).send(body);
    })
});

/**
 * @swagger
 * /api/order/deliver/:orderId:
 *   get:
 *      description: Delivers an order
 *      parameters:
 *        - name: orderId
 *      produces:
 *       - application/json
 *      responses:
 *        '200':
 *          description: The delivered order
 */
router.get(p.deliver.endpoint, async (req: Request, res: Response) => {
    const orderId = req.params.orderId;
    if (!orderId) {
        res.status(400).send('OrderId is required');
        return;
    }
    api.clientsDataByRoute.get(baseRoute)!.client.get(p.deliver.target.replace(":orderId",orderId)).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;
        res.status(status).send(body);
    })
});

/**
 * @swagger
 * /api/order/user/:userId:
 *   get:
 *      description: Gets all orders made by a user
 *      parameters:
 *        - name: userId
 *      produces:
 *       - application/json
 *      responses:
 *        '200':
 *          description: All orders belonging to a user
 */
router.get(p.getByUserId.endpoint, async (req: Request, res: Response) => {
    const userId = ""+ req.params.userId;
    if (!userId) {
        res.status(400).send('userId is required');
        return;
    }
    api.clientsDataByRoute.get(baseRoute)!.client.get(p.getByUserId.target.replace(":userId",userId)).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;
        res.status(status).send(body);
    })
});

/**
 * @swagger
 * /api/order/deliverer/:delivererId:
 *   get:
 *      description: Gets all orders accepted by a deliverer
 *      parameters:
 *        - name: delivererId
 *      produces:
 *       - application/json
 *      responses:
 *        '200':
 *          description: All orders accepted by a deliverer
 */
router.get(p.getByDelivererId.endpoint, async (req: Request, res: Response) => {
    const getByDelivererId = ""+ req.params.delivererId;
    if (!getByDelivererId) {
        res.status(400).send('DelivererId is required');
        return;
    }
    api.clientsDataByRoute.get(baseRoute)!.client.get(p.getByDelivererId.target.replace(":delivererId",getByDelivererId)).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;
        res.status(status).send(body);
    })
});

// Export default
export default {
    router,
    baseRoute
} as const;
