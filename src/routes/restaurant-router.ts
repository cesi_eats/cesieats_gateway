import StatusCodes from 'http-status-codes';
import express, { Request, Response, Router } from 'express';

import { ParamMissingError } from '@utils/errors';
import {ClientData} from "@routes/data/ClientData";
import api from "@routes/api";
import fileUpload from "express-fileupload";
import imageService from "@services/image-service";
import authService from "@services/auth-service";

const baseRoute = "/restaurant";



// Constants
const router = Router();
const { CREATED, OK } = StatusCodes;

router.use(express.static('public'));
router.use(fileUpload());


// Paths
export const p = {
    all: {endpoint : '/', target : '/api/restaurant/'},
    add: {endpoint : '/owner/', target : '/api/restaurant/'},
    get: {endpoint : '/:restaurantId', target : '/api/restaurant/:restaurantId'},
    update : {endpoint : '/owner/:restaurantId', target : '/api/restaurant/:restaurantId'},
    delete: {endpoint : '/:restaurantId', target : '/api/restaurant/:restaurantId'},
} as const;


//all routes beginning with /owner will need owner token
router.use('/owner', async function(req, res, next) {
    await api.isOwnerMiddleware(req, res, next);
});

/**
 * @swagger
 * /api/restaurant:
 *  get:
 *     description: Gets all restaurants
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: All restaurants
 */
router.get(p.all.endpoint, async (req: Request, res: Response) => {
    const id = req.params.id;
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.get(p.all.target).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    });
});

/**
 * @swagger
 * /api/restaurant/:restaurantId:
 *  get:
 *     description: Gets a restaurant by its id
 *     parameters:
 *       - name: cardId
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: The restaurant
 */
router.get(p.get.endpoint, async (req: Request, res: Response) => {

    const restaurantId = req.params.restaurantId;
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.get(p.get.target.replace(":restaurantId",restaurantId)).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    });
});


/**
 * @swagger
 * /api/restaurant/:
 *  post:
 *     description: Creates a restaurant
 *     parameters:
 *       - name: restaurantId
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: The created restaurant
 */
router.post(p.add.endpoint, async (req: Request, res: Response) => {

    //TODO check if admin or owner of the restaurant the article belongs to

    try {
        const body = JSON.parse(req.body.content);

        const imageResponse = await imageService.postImage(req);

        if (!imageResponse.success || !imageResponse.url){
            console.log("unable to upload image");
        }
        let requestBody;
        if (imageResponse.success || imageResponse.url){
            const url = imageResponse.url;
            requestBody= {
                ...body,
                img: url,
            }
        }
        else{
            requestBody= {
                ...body,
            }
        }
        api.clientsDataByRoute.get(baseRoute)!.client.post(p.add.target,requestBody).then((value: { body: object; }) => {
            // @ts-ignore
            const body = value.data;
            // @ts-ignore
            const status = value.status;

            res.status(status).send(body);
        });
    }
    catch (e){
        console.log(e);
        res.status(500).send({ success: false, message: "Server error" });
    }
});


/**
 * @swagger
 * /api/card/:restaurantId/:
 *  post:
 *     description: Updates a card by its id
 *     parameters:
 *       - name: cardId
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: The updated card
 */
router.put(p.update.endpoint, async (req: Request, res: Response) => {

    //TODO check if owner
    try {
        let body;
        if (req.body.content) {
            body = JSON.parse(req.body.content);
        }
        else{
            body = {};
        }
        const restaurantId = req.params.restaurantId;

        const imageResponse = await imageService.postImage(req);
        let requestBody;
        if (imageResponse.success || imageResponse.url){
            const url = imageResponse.url;
            requestBody= {
                ...body,
                img: url,
            }
        }
        else{
            requestBody= {
                ...body,
            }
        }
        // @ts-ignore
        api.clientsDataByRoute.get(baseRoute).client.put(p.update.target.replace(":restaurantId",restaurantId),requestBody).then((value: { body: object; }) => {
            // @ts-ignore
            const body = value.data;
            // @ts-ignore
            const status = value.status;

            res.status(status).send(body);
        }).catch((e: any) => {
            console.log(e);
        });
    }
    catch (e){
        console.log(e);
        res.status(500).send({ success: false, message: "Server error" });
    }
});



/**
 * @swagger
 * /api/category/:cardId/:
 *  delete:
 *     description: Deletes a card by its id
 *     parameters:
 *       - name: cardId
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 */
router.delete(p.delete.endpoint, async (req: Request, res: Response) => {

    //TODO check if admin or owner of the restaurant the category belongs to

    const restaurantId = req.params.restaurantId;
    // @ts-ignore
    api.clientsDataByRoute.get(baseRoute).client.delete(p.delete.target.replace(":restaurantId",restaurantId)).then((value: { body: object; }) => {
        // @ts-ignore
        const body = value.data;
        // @ts-ignore
        const status = value.status;

        res.status(status).send(body);
    });
});


// Export default
export default {
    router,
    baseRoute
} as const;
