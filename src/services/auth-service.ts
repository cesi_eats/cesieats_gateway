import { UserNotFoundError } from '@utils/errors';
import {Request} from "express";
import {p} from "@routes/auth-router";
import {Axios} from "axios";
import jwtDecode from 'jwt-decode';
import api from "@routes/api";


async function getAuthToken(req : Request) : Promise<string>{
    let token: any = req.headers.authorization && req.headers.authorization.match(/^Bearer (.*)$/);
    if (token && token[1]) { token = token[1]; }
    return token;
}

async function isAdmin(token : string) : Promise<boolean>{
    let decoded = jwtDecode(token);
    // @ts-ignore
    return decoded.role == 'admin';
}

async function isDeliverer(token : string) : Promise<boolean>{
    let decoded = jwtDecode(token);
    // @ts-ignore
    return decoded.role == 'deliverer';

}

async function isOwner(token : string) : Promise<boolean>{
    let decoded = jwtDecode(token);
    // @ts-ignore
    return decoded.role == 'owner';
}

async function isOwnerOfRestaurant(token : string, restaurantId : string) : Promise<boolean>{
    if (!await isOwner(token)) {
        return false;
    }
    let decoded = jwtDecode(token);
    // @ts-ignore
    return decoded.id == userId;
}


async function isValid(client : Axios, token : string) : Promise<boolean>{
    try {
        const response = await client.post(p.isValid.target, {
            token: token
        })
        return !!response.data.success;
    }
    catch (e){
        return false;
    }
}

// Export default
export default {
    getAuthToken,
    isDeliverer,
    isOwner,
    isAdmin,
    isOwnerOfRestaurant,
    isValid
} as const;
