import {p} from "@routes/images-router";
import FormData from "form-data";
import imagesClient from "@routes/data/setup";

interface UploadedImageResponse{
    success: boolean;
    url?: string;
    message?: string;
}

// @ts-ignore
async function postImage(req: any) : Promise<UploadedImageResponse>{

    const client = imagesClient.imagesClient.client;


    const { headers, files } = req;
    if (!files || !files['img']) {
        return {
            success: false,
            message: "No image provided"
        };
    }
    const { data, name } = files['img'];
    const formFile = new FormData();
    formFile.append('img', data,  name );
    headers['Content-Type'] = 'multipart/form-data';

    const result = await client.post(p.add.target, formFile, {}, headers).then((value: { body: any; }) => {
        // @ts-ignore
        const body = value.data;
        return {
            success: true,
            url: body.url
        };

    }).catch((err: any) => {
        console.log(err);
        return {success: false, message: "Error uploading image"};
    });
    return result;
}

// Export default
export default {
    postImage
} as const;
